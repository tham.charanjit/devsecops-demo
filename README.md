# Container Security - A DevSecOps Practical Example



## Introduction

As Docker containers adoption rapidly grows, security concerns turn into one of the top challenges, becoming increasingly important to better understand how to build, configure and deploy secure containerized applications. The Center for Internet Security (CIS) published a Docker Benchmark [[1](https://www.cisecurity.org/benchmark/docker/)], which provides a consensus-driven security guideline by subject matter experts for users and organizations to achieve secure Docker usage and configuration. Having this in mind, this repository provides a simple example of a CI/CD pipeline that adds a layer of security to the traditional building process of a Docker container, by introducing inspection mechanisms that can help with some sections of the CIS Docker Benchmark.



## Objective

The man goal here is to implement a pipeline that builds a container with a simple Java spring boot application, while performing security inspections over the newly created containers. The general workflow of the process is represented in the picture below:

![workflow](.readme/workflow.png)

The pipeline itself comprises the following stages:

* **Build**: compiles the source code and builds a new Docker container image;
* **Inspection**: runs a series of tests to inspect the newly created image for security issues; policy-based assess if the container image is considered secure or not;
* **Decision**: if the newly created container image passes the security inspection the next stage is executed, otherwise the pipeline also fails and the process is aborted;
* **Registry**: when reached, it means the new Docker container image passed the security inspection tests, thus considered ready to be pushed to the final Docker Registry;
* **Report**: manages the reports of the security inspections done over the newly created container image, and will be reached no matter the outcome of said security inspections - pass or fail.



## Inspection Engine

For this proof of concept Anchore [[2](https://anchore.com/about)] will be used on the pipeline as its Inspection Engine. It's a comprehensive open-source container security inspection platform that performs an in-depth analysis of container images, generating detailed manifests and creating and applying specific policy gates and checks for the entire container workload. It follows these engine concepts:

- Perform **image analysis** and store results, mapped to container image content ID;
- Consume **data feeds** from a variety of external sources;
- Execute **policy evaluations** that run a series of checks, and result in pass/fail recommendations;
- Perform  **security scans** that generate software vulnerability reports;
- Generate **notifications** when above processes result in important events.

Policies are central to the concept of Anchore Engine. The Anchore article, "Working with Policies" [[3](https://anchore.freshdesk.com/support/solutions/articles/36000003885-working-with-policies)], provides information on how to create, delete, update, and describe policies using the Anchore CLI to interact with a running Anchore Engine deployment. 

At an high-level, Anchore Engine consumes policies stored in a *Policy Bundle* [[4](https://anchore.freshdesk.com/support/solutions/articles/36000074705-policy-bundles-and-evaluation)] that are just JSON documents used for the security evaluations, with the following properties and/or abilities:

- **Image checks**:
  - OS packages (rpm, deb, apk);
  - 3rd party packages (gem, npm, java, py);
  - file names and content;
  - build metadata (Dockerfile).

- **Security checks**:
  - software vulnerabilities (OS and 3rd party packages);
  - secrets/keys search;
  - metadata checks (root user, exposed ports, health-checks).

- **Customizable**:
  - apply different policies at each stage of container life-cycle;
  - users/clients can create customizable policies based on their individual needs.

The Policy Bundle used for this proof of concept is defined in the `policy_bundle.json` file. Without getting into the technical details, the policies defined in the bundle can be summarized as this:

- **Package Blacklisting**: the `openssh` package is blacklisted;
- **Effective User:** the effective user cannot be the `root` user;
- **Exposed Ports**: the ports `22`, `80` and `443` cannot be exposed;
- **Healthcheck**: the container must include a health-check directive;
- **CVE Vulnerabilities:** if vulnerabilities with *critical* or *high* severity are found the inspection fails, and if vulnerabilities with *medium* or *low* severity are found a warning will be issued instead;

A bundle evaluation results in a status of *pass* or *fail*. A *pass* status means the image evaluated against the bundle and only *go or warn* actions resulted from the policy evaluation and whitelist evaluations, or the image was whitelisted. A *fail* status means the image evaluated against the bundle and at least one *stop* action resulted from the policy evaluation and whitelist evaluation, or the image was blacklisted [[1](<https://anchore.freshdesk.com/support/solutions/articles/36000074705-policy-bundles-and-evaluation>)].



## Demo Procedure

The first step is to fork this repository into your GitLab account [[2](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html)] and clone it [[3](<https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository>)]. After that you'll need to get into the project's settings and define three environment variables (Settings > CI/CD > Environment Variables > Expand) [[4](https://gitlab.com/help/ci/variables/README#variables)]:

![](.readme/cicd_env_vars.png)

These three variables are your Docker Hub's [[5](https://hub.docker.com/)] username, password and the respective repository where the pipeline should push the docker images to.  Without these three variables, the "Registry Stage" of the pipeline will fail, as it will be unable to publish the image. <u>Please do not include the username and the slash</u> on the repository environment variable, as those will be added automatically by the pipeline script.

After following theses two steps you should be able to start the pipeline for the first time (CI/CD > Pipelines > Run Pipeline). The next pipeline runs are triggered automatically on each new commit pushed to the source repository.

#### Security Inspection Failure

The first time you run the pipeline it should fail. A single policy of the bundle producing a `STOP` action will be enough for the inspection to end up in a `FAIL` condition. The *Inspection Stage* itself shouldn't have any problem as well as the *Decision Stage*, and both should be marked as passed and in green color. The only stage that should be marked as failed and in red the is the *Registry Stage*.

![inspection_fail](.readme/cicd_fail_1.png)

![inspection_fail](.readme/cicd_fail_2.png)

This happens because the inspection procedure itself ran properly until the end, despite the inspection of the container image ended up with a fail condition. The inspection reports produced on the *Inspection Stage* were then used on the *Decision Stage*, determining that the newly created image shouldn't be published to the final Docker registry, leading then to the failure of *Registry Stage*.

The *Reports Stage* hold the security inspection reports that can be downloaded for your convenience. There is one specific file that can help us to understand why the inspection failed, the `build_master-*-policy.json`, which has a `"status": "fail"`. The `"result"` section of the document lists all the results of the inspection that led to the final status of failure, which can be summarized as follows:

- "Package is blacklisted: openssh";
- "Dockerfile exposes port (22) which is in policy file DENIEDPORTS list";
- "Dockerfile directive 'HEALTHCHECK' not found, matching condition 'not_exists' check"
- "User root found as effective user, which is explicitly not allowed list";
- "Several CVE vulnerabilities with high or critical severity".

The first four problems are related to directives of the Dockerfile (or lack of them). The `RUN apk add openssh` instruction leads to the first problem as it installs the blacklisted package, the `EXPOSE 22` instruction leads to the second one, not creating an healthcheck directive leads to the third problem, and not enforcing an effective user makes *root* the default one, leading to the fourth problem. The last problem comes from the usage of outdated Java libraries that contain well known vulnerabilities.


#### Security Inspection Pass

Faced with a security inspection failure and a consequent Docker container image rejection, one needs to fix the problems raised by the security inspection to succeed with the automated image build and the respective policy compliance. 

To simplify the process, the repository already includes the files with the required changes to fix the problems detected by the security inspection: `Dockerfile.pass` and `pom.xml.pass`. Lets compare them to their counterparts and analyze the changes that will enable the new container to comply with the defined policies and to finally be pushed to the Docker registry.

Starting with the `Dockerfile`, the changes are the following ones:

- **Line 7**: the directive to install the SSH server is removed;
- **Lines 8-9**: directives to create a new user and set it as the effective user are added;
- **Lines 11-12**: an HEALTHCHECK directive is added to the container (arguably not the best one tough [[6](https://blog.sixeyed.com/docker-healthchecks-why-not-to-use-curl-or-iwr/)]);
- **Line 16**: the directive exposing port 22 is removed.

![dockerfile_diff](.readme/diff_dockerfile.png)

The changes in the `pom.xml` files are straightforward in this case. All that is required to fix the most severe vulnerabilities detected by the inspection is a change of version on the Spring Boot framework to the latest one available, 2.1.3 at the moment.

![pom_diff](.readme/diff_pom.png)



Now that we've already taken a look at the changes on the files, just copy both of them over the original ones (e.g., `cp Dockerfile.pass Dockerfile`). Afterwards commit and push [[7](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-and-commit-local-changes)] the changes to the repository to trigger a new run of the pipeline, this time around with the problems fixed and if everything goes as expected, the newly created container should pass the security inspection and all the stages of the pipeline should be marked as *passed* and green colored.

![inspection_fail](.readme/cicd_pass_1.png)

![inspection_fail](.readme/cicd_pass_2.png)

This means that Docker container image has finally been considered secure, according to the specified policies, and thus pushed to the final Docker registry. Check the Docker Hub repository that you specified previously and the Docker image should be there, as shown below:

![pom_diff](.readme/cicd_dockerhub.png)



## Closing Thoughts

The proof of concept presented here is just an example on how to achieve an automated process of building Docker containers images which are more secure, by following DevSecOps [[8](https://www.redhat.com/en/topics/devops/what-is-devsecops)] [[9](https://anchore.com/blog/what-is-devsecops/)] practices and consensually recognized security guidelines. The technologies involved here may not necessarily be the best or the only ones. Although Anchore Engine was chosen as an example, other technologies could be used in its place or even to complement it at the Inspection Stage of the pipeline.

Also, regarding the CIS Docker Benchmark, only a fraction of the definitions included in the guideline were taken into account, namely those that are related to the container images and build files [[10](https://anchore.com/blog/cis-docker-benchmark/)] . The security regarding the configuration of the host and the Docker daemon should also be considered, but that was left out of the scope of this exercise. Docker published a script [[11](https://github.com/docker/docker-bench-security)] that checks for dozens of common best-practices around deploying Docker containers in production inspired by the CIS Docker Benchmark that I really recommend having a look into.

However simple the procedure exemplified here may be, I hope it can be of any help on your endeavour while pursuing the adoption and integration of better security practices and processes to achieve a more secure development process as a whole.



## References

1. [CIS Docker Benchmark](https://www.cisecurity.org/benchmark/docker/)
2. [Anchore Solutions](https://anchore.com/solutions/)
3. [Anchore - Working with Policies](https://anchore.freshdesk.com/support/solutions/articles/36000003885-working-with-policies)
4. [Anchore - Policy Bundles and Evaluation](https://anchore.freshdesk.com/support/solutions/articles/36000074705-policy-bundles-and-evaluation)
5. [Docker Hub](https://hub.docker.com/)
6. [Docker Healthchecks: Why Not To Use `curl` or `iwr`](https://blog.sixeyed.com/docker-healthchecks-why-not-to-use-curl-or-iwr/)
7. [GitLab Basics](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-and-commit-local-changes)
8. [RedHat - What is DevSecOps?](https://www.redhat.com/en/topics/devops/what-is-devsecops)
9. [Anchore Blog - What is DevSecOps](https://anchore.com/blog/what-is-devsecops/)
10. [Anchore Blog - Using Anchore Policies to Help Achieve the CIS Docker Benchmark](https://anchore.com/blog/cis-docker-benchmark/)
11. [Docker Bench for Security](https://github.com/docker/docker-bench-security)
12. [Anchore - Gitlab Integration](https://anchore.freshdesk.com/support/solutions/articles/36000058199-gitlab-integration)
